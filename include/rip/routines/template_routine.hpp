#ifndef TEMPLATE_ROUTINE_TEMPLATE_ROUTINE_HPP
#define TEMPLATE_ROUTINE_TEMPLATE_ROUTINE_HPP

#include <rip/routine.hpp>

namespace rip::routines 
{

    class TemplateRoutine : public Routine
    {
        public:

            TemplateRoutine(const nlohmann::json &config, std::shared_ptr<EventSystem> es, std::string id,
                    std::shared_ptr<std::unordered_map<std::string, 
                    std::shared_ptr<RobotComponent> > > comps);

            virtual void start(std::vector<std::any> data = {}) override;

            virtual void stop(std::vector<std::any> data = {}) override;

        protected:

            virtual void saveComponents(std::shared_ptr<std::unordered_map<std::string,
                    std::shared_ptr<RobotComponent> > > comps) override;

            virtual void run() override;
    };

}

#endif // TEMPLATE_ROUTINE_TEMPLATE_ROUTINE_HPP
